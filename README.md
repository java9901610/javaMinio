# Java with minio

## Run minio for local purposes:
```bash
podman run -p 9000:9000 -p 9001:9001 \
  --name minio \
  -d quay.io/minio/minio server /data --console-address ":9001" \
  && podman logs minio
```

- API: `http://10.0.2.100:9000  http://127.0.0.1:9000`
- Console: `http://10.0.2.100:9001 http://127.0.0.1:9001`
- Default credentials: `minioadmin:minioadmin`

Create the access key:  
go to `http://127.0.0.1:9000` -> Access Keys -> Create access key -> (policy on/off) -> Create
Write these down, as this is the only time the secret will be displayed:
```
Access Key: yM4wi6byxxxxxxx
Secret Key: 3TOr2UGaXyvqtDFxEK5Sxxxxxxxxxxxx
```

> - Install minio for java with [this link](https://min.io/docs/minio/linux/developers/minio-drivers.html#java-sdk)
> - Also: [https://min.io/docs/minio/linux/developers/java/minio-java.html](https://min.io/docs/minio/linux/developers/java/minio-java.html)
> - Quickstart: [https://min.io/docs/minio/linux/developers/java/minio-java.html#quick-start-example-file-uploader](https://min.io/docs/minio/linux/developers/java/minio-java.html#quick-start-example-file-uploader)
> - Examples with java: [https://github.com/minio/minio-java/tree/release/examples](https://github.com/minio/minio-java/tree/release/examples)
> - API: [https://github.com/minio/minio-java/blob/master/docs/API.md](https://github.com/minio/minio-java/blob/master/docs/API.md)

## Create the project:
```bash
sudo mkdir /opt/gradle
curl -L https://downloads.gradle-dn.com/distributions/gradle-7.6-bin.zip -o /tmp/gradle-7.6-bin.zip
sudo unzip -d /opt/gradle /tmp/gradle-7.6-bin.zip
export PATH=$PATH:/opt/gradle/gradle-7.6/bin
mkdir javaMinio
cd javaMinio
gradle -v
gradle init (2,3->1,DSL:1,no,4)
gradle tasks
tree -a .
./gradlew tasks --all
./gradlew build
export ACCESS_KEY=yM4wi6byxxxxxxx
export SECRET_KEY=3TOr2UGaXyvqtDFxEK5Sxxxxxxxxxxxx
./gradlew run
```
