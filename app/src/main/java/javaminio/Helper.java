package javaminio;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Helper {
    final String lorem = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n";

    void genFile(String path) {
        try {
            File f = new File(path, "awesome.log");
            FileWriter myWriter = new FileWriter(f);
            myWriter.write(this.lorem);
            myWriter.close();
            System.out.println(String.format("Successfully wrote to the file: %s/awesome.log", path));
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
