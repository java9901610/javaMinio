/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package javaminio;

import io.minio.MinioClient;
import io.minio.errors.*;
import io.minio.messages.Bucket;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App {

    // For Inteleji: Put into `Run` -> `Edit Configuration` these variables: ACCESS_KEY=yM4wi6byxxxxxxx;SECRET_KEY=3TOr2UGaXyvqtDFxEK5Sxxxxxxxxxxxx
    public static final String accessKey = System.getenv("ACCESS_KEY");
    public static final String secretKey = System.getenv("SECRET_KEY");

    public static void main(String[] args)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        try {
            final int targetStringLength = 4;

            /* play.min.io for test and development. */
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint("http://localhost:9000")
                            .credentials(accessKey, secretKey)
                            .build();

            // generate the file
            Helper gf = new Helper();
            gf.genFile("/tmp");

            Minio m = new Minio();

            m.lsBuckets(minioClient);

            System.out.println(m.existBucket(minioClient, "mybucket1"));

            String prefix = m.genStr(targetStringLength);
            String bucketName = String.format("%s-my-awesome-bucket", prefix);
            m.createBucket(minioClient, bucketName);

            Map<String, String> tags = new HashMap<>();
            tags.put("Project", "checkTags");
            tags.put("Firstname", "John");
            tags.put("Lastname", "Smith");

            Map<String, String> metaData = new HashMap<>();
            metaData.put("Content-Type", "application/octet-stream");
            metaData.put("X-Amz-Meta-Testing", "1234");
            metaData.put("example", "5678");
            metaData.put("X-Amz-Meta-Firstname", "Peter");
            metaData.put("X-Amz-Meta-Lastname", "Shulz");
            metaData.put("X-Amz-Meta-Type", "patient");

            Map<String, String> metaDataAlso = new HashMap<>();
            metaDataAlso.put("example2", "101112");

            m.uploadFile(minioClient, bucketName, "111/222/minikube1.log", "/tmp/awesome.log", tags, metaData, metaDataAlso);
            m.uploadFile(minioClient, bucketName, "111/444/minikube2.log", "/tmp/awesome.log", tags, metaData, metaDataAlso);
            m.uploadFile(minioClient, bucketName, "111/minikube3.log", "/tmp/awesome.log", tags, metaData, metaDataAlso);
            m.uploadFile(minioClient, bucketName, "111/minikube4.log", "/tmp/awesome.log", tags, metaData, metaDataAlso);
            m.uploadFile(minioClient, bucketName, "minikube5.log", "/tmp/awesome.log", tags, metaData, metaDataAlso);
            m.uploadFile(minioClient, bucketName, "minikube6.log", "/tmp/awesome.log", tags, metaData, metaDataAlso);

            m.getTags(minioClient, bucketName);
            m.getObjTags(minioClient, bucketName, "111/222/minikube1.log");
            m.getObjMeta(minioClient, bucketName, "111/222/minikube1.log");
            m.listObjects(minioClient, bucketName);

            // remove after 15 second
            Thread.sleep(15000);
            m.removeFile(minioClient, bucketName, "111/222/minikube1.log");
            m.removeFile(minioClient, bucketName, "111/444/minikube2.log");
            m.removeFile(minioClient, bucketName, "111/minikube3.log");
            m.removeFile(minioClient, bucketName, "111/minikube4.log");
            m.removeFile(minioClient, bucketName, "minikube5.log");
            m.removeFile(minioClient, bucketName, "minikube6.log");
            m.removeBucket(minioClient, bucketName);

//      exit with 0
            if (true) {
                System.exit(0);
            }
//      1
            List<Bucket> bucketList = minioClient.listBuckets();
            for (Bucket bucket : bucketList) {
                System.out.println(bucket.creationDate() + ", " + bucket.name() + ", listBuckets from main");
            }
//        2
            lsBuckets(minioClient);
//        3
            Minio1 m1 = new Minio1();
            m1.lsBuckets(minioClient);
//        4
            Minio2 m2 = new Minio2();
            m2.lsBuckets(minioClient);
        } catch (MinioException e) {
            System.out.println("Error occurred: " + e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    //  2
    static void lsBuckets(@NotNull MinioClient mc) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        for (Bucket bucket : mc.listBuckets()) {
            System.out.println(bucket.creationDate() + ", " + bucket.name() + ", lsBuckets from App");
        }
    }
}

//3
class Minio1 {
    void lsBuckets(@NotNull MinioClient mc) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        for (Bucket bucket : mc.listBuckets()) {
            System.out.println(bucket.creationDate() + ", " + bucket.name() + ", lsBuckets from Minio1");
        }
    }
}
