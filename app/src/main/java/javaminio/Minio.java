package javaminio;

import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.Bucket;
import io.minio.messages.Item;
import io.minio.messages.Tags;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Minio {
    String genStr(int targetStringLength) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    void lsBuckets(@NotNull MinioClient mc) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        List<Bucket> buckets;
        buckets = mc.listBuckets();
        System.out.print("List of buckets:\n");
        for (Bucket elem : buckets) {
            System.out.println(elem.creationDate() + " ::: " + elem.name());
        }
    }

    // Check whether 'mybucket1' exist or not.
    String existBucket(@NotNull MinioClient mc, String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        boolean exists = mc.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build());
        if (exists) {
            return String.format("Bucket %s exists", bucketName);
        } else {
            return String.format("Bucket %s doesn't exists", bucketName);
        }
    }

    // Make bucket bucketName if doesn't exist
    void createBucket(@NotNull MinioClient mc, String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        if (
                mc.bucketExists(BucketExistsArgs.builder().bucket(bucketName).build())
        ) {
            System.out.println("Bucket %s already exists" + bucketName);
        } else {
            mc.makeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
            Map<String, String> bucketTags = new HashMap<>();
            bucketTags.put("my-bucket-tag1", "value-of-bucket-tag1");
            bucketTags.put("my-bucket-tag2", "value-of-bucket-tag2");
            mc.setBucketTags(SetBucketTagsArgs.builder().bucket(bucketName).tags(bucketTags).build());

        }
    }

    void uploadFile(@NotNull MinioClient mc, String bucketName, String object, String filename, Map<String, String> tags, Map<String, String> metaData, Map<String, String> metaDataAlso) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        mc.uploadObject(
                UploadObjectArgs.builder()
                        .bucket(bucketName)
                        .object(object)
                        .filename(filename)
                        .tags(tags)
                        .headers(metaData)
                        .userMetadata(metaDataAlso)
                        .build());
        System.out.printf("%s is successfully uploaded as %s to %s%n", filename, object, bucketName);
    }

    void getTags(@NotNull MinioClient mc, String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        Tags tags = mc.getBucketTags(GetBucketTagsArgs.builder().bucket(bucketName).build());
        System.out.printf("%s has these tags:\n %s%n", bucketName, tags.get());
    }

    void getObjTags(@NotNull MinioClient mc, String bucketName, String object) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        Tags tags = mc.getObjectTags(GetObjectTagsArgs.builder().bucket(bucketName).object(object).build());
        System.out.printf("%s/%s has these tags:\n %s%n", bucketName, object, tags.get());
    }

    void getObjMeta(@NotNull MinioClient mc, String bucketName, String object) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        System.out.printf("User meta data of %s/%s:\n%s", bucketName, object, mc.statObject(StatObjectArgs.builder()
                .bucket(bucketName)
                .object(object)
                .build()).userMetadata());
        System.out.printf("Get header `x-amz-meta-firstname` from %s/%s:\n%s%n", bucketName, object, mc.statObject(StatObjectArgs.builder()
                .bucket(bucketName)
                .object(object)
                .build()).headers().get("x-amz-meta-firstname"));
        System.out.printf("Return value of key `example2` of object %s/%s:\n%s%n", bucketName, object, mc.statObject(StatObjectArgs.builder()
                .bucket(bucketName)
                .object(object)
                .build()).userMetadata().get("example2"));
        System.out.printf("Last modified date of %s/%s:\n%s%n", bucketName, object, mc.statObject(StatObjectArgs.builder()
                .bucket(bucketName)
                .object(object)
                .build()).lastModified());
        System.out.printf("Info about object %s/%s:\n%s%n", bucketName, object, mc.statObject(StatObjectArgs.builder()
                .bucket(bucketName)
                .object(object)
                .build()));
    }

    void listObjects(@NotNull MinioClient mc, String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        // https://github.com/minio/minio-java/blob/master/docs/API.md#listobjectslistobjectsargs-args
        Iterable<Result<Item>> objects = mc.listObjects(
                ListObjectsArgs.builder()
                        .bucket(bucketName)
                        .recursive(true)
                        .prefix("111")
                        .maxKeys(10)
                        .build());
        System.out.println("Got max 10 objects from / with prefix 111 recursively:");
        for (Result<Item> elem : objects) {
            System.out.printf("\t- %s\n", elem.get().objectName());
        }
    }

    void removeFile(@NotNull MinioClient mc, String bucketName, String object) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        mc.removeObject(
                RemoveObjectArgs.builder()
                        .bucket(bucketName)
                        .object(object)
                        .build());
        System.out.printf("%s is successfully removed from %s%n", object, bucketName);
    }

    void removeBucket(@NotNull MinioClient mc, String bucketName) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        mc.removeBucket(
                RemoveBucketArgs.builder()
                        .bucket(bucketName)
                        .build());
        System.out.printf("%s is successfully removed%n", bucketName);
    }
}
