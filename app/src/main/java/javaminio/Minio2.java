package javaminio;

import io.minio.MinioClient;
import io.minio.errors.*;
import io.minio.messages.Bucket;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

// 4
class Minio2 {
    void lsBuckets(@NotNull MinioClient mc) throws ServerException, InsufficientDataException, ErrorResponseException, IOException, NoSuchAlgorithmException, InvalidKeyException, InvalidResponseException, XmlParserException, InternalException {
        for (Bucket bucket : mc.listBuckets()) {
            System.out.println(bucket.creationDate() + ", " + bucket.name() + ", lsBuckets from Minio2");
        }
    }
}